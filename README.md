# metadata templates

The goal of the metadata templates package is to provide guidelines for the annotation of sequencing data. The designed template is blended from various metadata templates derived from the National Institutes of Health (NIH) National Center for Biotechnology Information (NCBI) data repositories: Gene Expression Omnibus (GEO) and Sequence Read Archive (SRA). Specifically, this template is focused on submissions to GEO, and thus are geared primarily for this purpose. Furthermore, the template is designed for use primarily for model organisms (such as mus musculus) and humans. The template provides templates to annotate experiment and biosample level characteristics, documentation and examples for all required and additional optional fields, and additional guidelines for proper formatting of fields. 

## Usage

The template is organized into an Microsoft Excel (.xlsx) format file, making use of cell highlighting and formatting to increase readability. The Excel file contains several sheets, which includes summary of the template file, term definitions, and finally the experiment/sample characteristics to be used for long-term storage and final data submission to public repositories (GEO). 

## Feedback

Feedback always appreciated, please file an issue via [Bitbucket](https://bitbucket.com/robert_amezquita/metadata_templates), and feel free to share, fork, or submit pull requests. 
